﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class REST : MonoBehaviour
{
    private const string IEX_API_KEY = "sk_b4b044fc62904017833648baaadf7d0e";

    public GameObject gameData;
    private SceneControl sceneController;

    public GameObject answers;

    public bool chat = false;

    private void Start()
    {
        gameData = GameObject.FindGameObjectWithTag("GameController");
    }

    public IEnumerator GetQnAToken()
    {
        UnityWebRequest www = new UnityWebRequest("http://18.191.38.250/token");
        www.downloadHandler = new DownloadHandlerBuffer();

        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            // Show results as text
            Debug.Log(www.downloadHandler.text);

            string token = www.downloadHandler.text;
            token = token.Replace(@"\r\n?|\n", "");
            token = token.Replace("\n", "");

            gameData.GetComponent<GameData>().tokenQnA = token;
        }
    }

    public void RunGetStockData(GameObject gameObject)
    {
        StartCoroutine(GetStockData(gameObject.GetComponent<InputField>().text));
    }

    public IEnumerator GetStockData(string stockName)
    {
        //Use IEX API to get full stock data
        UnityWebRequest www = new UnityWebRequest("http://18.191.38.250/stockData.php/" + stockName);
        www.downloadHandler = new DownloadHandlerBuffer();

        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            // Show results as text
            Debug.Log(www.downloadHandler.text);

            RunGetIEXData(www.downloadHandler.text);
        }
    }

    public void RunGetIEXData(string symbol)
    {
        StartCoroutine(GetIEXData(symbol));
    }

    public IEnumerator GetIEXData(string stockName)
    {
        GameObject tempGameObject = GameObject.FindGameObjectWithTag("SceneController");
        sceneController = tempGameObject.GetComponent<SceneControl>();

        Debug.Log(stockName);

        //Use IEX API to get full stock data
        UnityWebRequest www = new UnityWebRequest("https://cloud.iexapis.com/stable/stock/" + stockName + "/quote/?token=sk_b4b044fc62904017833648baaadf7d0e");
        www.downloadHandler = new DownloadHandlerBuffer();

        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            // Show results as text
            Debug.Log(www.downloadHandler.text);

            gameData.GetComponent<GameData>().fullStockData = new ArrayList();

            gameData.GetComponent<GameData>().fullStockData.Add(www.downloadHandler.text.Substring(www.downloadHandler.text.IndexOf("close\":") + "close\":".Length));
            gameData.GetComponent<GameData>().fullStockData.Add(www.downloadHandler.text.Substring(www.downloadHandler.text.IndexOf("open\":") + "open\":".Length));
            gameData.GetComponent<GameData>().fullStockData.Add(www.downloadHandler.text.Substring(www.downloadHandler.text.IndexOf("marketCap\":") + "marketCap\":".Length));
            gameData.GetComponent<GameData>().fullStockData.Add(www.downloadHandler.text.Substring(www.downloadHandler.text.IndexOf("peRatio\":") + "peRatio\":".Length));
            gameData.GetComponent<GameData>().fullStockData.Add(www.downloadHandler.text.Substring(www.downloadHandler.text.IndexOf("week52High\":") + "week52High\":".Length));
            gameData.GetComponent<GameData>().fullStockData.Add(www.downloadHandler.text.Substring(www.downloadHandler.text.IndexOf("volume\":") + "volume\":".Length));
            gameData.GetComponent<GameData>().fullStockData.Add(www.downloadHandler.text.Substring(www.downloadHandler.text.IndexOf("avgTotalVolume\":") + "avgTotalVolume\":".Length));
            gameData.GetComponent<GameData>().fullStockData.Add(www.downloadHandler.text.Substring(www.downloadHandler.text.IndexOf("companyName\":") + "companyName\":".Length));
            gameData.GetComponent<GameData>().fullStockData.Add(www.downloadHandler.text.Substring(www.downloadHandler.text.IndexOf("symbol\":") + "symbol\":".Length));

            for (int i = 0; i < 9; i++)
            {
                gameData.GetComponent<GameData>().fullStockData[i] = gameData.GetComponent<GameData>().fullStockData[i].ToString().Substring(0,
                    gameData.GetComponent<GameData>().fullStockData[i].ToString().IndexOf(","));

                gameData.GetComponent<GameData>().fullStockData[i] = gameData.GetComponent<GameData>().fullStockData[i].ToString().Replace("\"", "");
            }

            if (chat)
            {
                chat = false;
                answers.GetComponent<UnityEngine.UI.Text>().text = "The current market price for " + stockName + " is " + gameData.GetComponent<GameData>().fullStockData[0];
            }
            else
            {
                // Chane Scene to show stock info
                sceneController.ChangeScene("Ticker Screen");
            }
        }
    }

    public void RunQnASearch(GameObject gameObject)
    {
        StartCoroutine(SearchQnA(gameObject.GetComponent<InputField>().text));
    }

    public IEnumerator SearchQnA(string query)
    {
        Debug.Log(Uri.EscapeUriString(query));

        UnityWebRequest www = new UnityWebRequest("http://18.191.38.250/searchQnA.php/" + Uri.EscapeUriString(query));
        www.downloadHandler = new DownloadHandlerBuffer();

        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            // Show results as text
            Debug.Log(www.downloadHandler.text);

            if (www.downloadHandler.text != "The current market price for [ticker] is [price]")
            {
                Debug.Log(www.downloadHandler.text);

                answers.GetComponent<UnityEngine.UI.Text>().text = www.downloadHandler.text;
            } 
            else
            {
                chat = true;
                StartCoroutine(GetStockData(query));
            }
        }
    }
}
