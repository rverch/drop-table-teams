﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneControl : MonoBehaviour
{
    void Start()
    {
        if (GameObject.Find("SceneController(Clone)") != null)
        {
            Destroy(this);
        }
        else
        {
            DontDestroyOnLoad(this);
        }
    }

    public void ChangeScene(string newScene)
    {
        SceneManager.LoadScene(newScene);
    }

    public void GetScene()
    {
        SceneManager.GetActiveScene();
    }

    public bool CheckCurrScene(string scene)
    {
        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName(scene))
        {
            return true;
        }

        return false;
    }
}
