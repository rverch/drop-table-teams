﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameData : MonoBehaviour
{
    public string tokenQnA;

    public REST rest;

    private float timeInterval; // interval in seconds
    private float actualTime;

    public ArrayList fullStockData = new ArrayList();

    // Start is called before the first frame update
    void Start()
    {
        if (GameObject.Find("GameData(Clone)") != null)
        {
            Destroy(this);
        }
        else
        {
            DontDestroyOnLoad(this);
        }

        timeInterval = 300.0f; // set interval for 5 minutes
        actualTime = timeInterval; // set actual time left until next update

        StartCoroutine(rest.GetQnAToken());
    }

    // Update is called once per frame
    void Update()
    {
        actualTime -= Time.deltaTime; // subtract the time taken to render last frame

        if (actualTime <= 0) // if time runs out, do your update
        {
            StartCoroutine(rest.GetQnAToken());

            actualTime = timeInterval; // reset the timer
        }
    }
}
