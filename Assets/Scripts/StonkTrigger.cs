﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StonkTrigger : MonoBehaviour
{
    public GameObject popup;

    private bool pressed = false;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnMouseDown()
    {
        TriggerBtnPress();
    }

    public void TriggerBtnPress()
    {
        if (pressed)
        {
            popup.SetActive(false);

            pressed = !pressed;
        } 
        else
        {
            popup.SetActive(true);

            pressed = !pressed;
        }
    }
}
