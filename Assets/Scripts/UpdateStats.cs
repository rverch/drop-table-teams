﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateStats : MonoBehaviour
{
    public GameObject toMove;
    private GameData gameData;

    // Start is called before the first frame update
    void Start()
    {
        GameObject tempGameObject = GameObject.FindGameObjectWithTag("GameController");
        gameData = tempGameObject.GetComponent<GameData>();

        GameObject previous_close = GameObject.FindGameObjectWithTag("previous_close");
        previous_close.GetComponent<UnityEngine.UI.Text>().text = gameData.fullStockData[0].ToString();

        GameObject open = GameObject.FindGameObjectWithTag("open");
        open.GetComponent<UnityEngine.UI.Text>().text = gameData.fullStockData[1].ToString();

        GameObject market_capitilization = GameObject.FindGameObjectWithTag("market_cap");
        market_capitilization.GetComponent<UnityEngine.UI.Text>().text = gameData.fullStockData[2].ToString();

        GameObject peRatio = GameObject.FindGameObjectWithTag("peRatio");
        peRatio.GetComponent<UnityEngine.UI.Text>().text = gameData.fullStockData[3].ToString();

        GameObject week = GameObject.FindGameObjectWithTag("52week");
        week.GetComponent<UnityEngine.UI.Text>().text = gameData.fullStockData[4].ToString();

        GameObject volume = GameObject.FindGameObjectWithTag("volume");
        volume.GetComponent<UnityEngine.UI.Text>().text = gameData.fullStockData[5].ToString();

        GameObject avgVolume = GameObject.FindGameObjectWithTag("avg_volume");
        avgVolume.GetComponent<UnityEngine.UI.Text>().text = gameData.fullStockData[6].ToString();

        GameObject compName = GameObject.FindGameObjectWithTag("comp_name");
        compName.GetComponent<UnityEngine.UI.Text>().text = gameData.fullStockData[7].ToString() + " (" + gameData.fullStockData[8].ToString() + ")";
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
